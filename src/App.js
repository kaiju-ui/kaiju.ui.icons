import React, { Component } from 'react';
import IconList from './components/IconList';
import basicIsons from './data/basic';

class App extends Component {
  render() {
    return (
      <div className="app">
        <IconList icons={basicIsons} title="Basic" spriteName="basic" />
      </div>
    );
  }
}

export default App;
