import React, {Component} from 'react';
import './index.css';
import IconItem from '../IconItem';

class IconList extends Component {
    render() {
        const { icons, title, spriteName } = this.props;

        return (
            <div className="icon-list">
                <h2 className="icon-list__title">{title} <span className="icon-list__subtitle">{icons.length}</span></h2>
                <ul className="icon-list__list">
                    {icons.map((id) => (
                        <li className="icon-list__item">
                            <IconItem
                                id={id}
                                spriteName={spriteName}
                                key={id}
                            />
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default IconList;