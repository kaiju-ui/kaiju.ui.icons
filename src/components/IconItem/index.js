import React, {Component} from 'react';
import './index.css';

class IconItem extends Component {
    render() {
        const { id, spriteName } = this.props;
        const src = `icons-sprite-${spriteName}.svg#${id}`;

        return (
            <div className="icon-item">
                <div className="icon-item__graphics">
                    <svg className='icon'>
                        <use xmlnsXlink='http://www.w3.org/1999/xlink' xlinkHref={src} />
                    </svg>
                </div>
                <div className="icon-item__title">{id}</div>
            </div>
        );
    }
}

export default IconItem;