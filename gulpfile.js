let gulp = require('gulp');
let svgSprite = require('gulp-svg-sprite');
let plumber = require('gulp-plumber');

let svgGlob = '**/*.svg'; // <-- Glob to match your SVG files

let getConfig = function (spriteName) {
    return {
        "dest": "dist",
        "mode": {
            "symbol": {
                "dest": "",
                "sprite": "icons-sprite-" + spriteName + ".svg"
            }
        }
    };
}

let spriteGenerator = function (spriteName) {
    return gulp.src(svgGlob, { cwd: 'src/icons/' + spriteName })
        .pipe(plumber())
        .pipe(svgSprite(getConfig(spriteName))).on('error', function(error){ console.log(error); })
        .pipe(gulp.dest('public'));
}

gulp.task('icons-basic', function() {
    return spriteGenerator('basic');
});

gulp.task('icons-sports', function() {
    return spriteGenerator('sports');
});

gulp.task('icons-categories', function() {
    return spriteGenerator('categories');
});

gulp.task('default', gulp.series(
    'icons-basic',
    'icons-sports',
    'icons-categories'
));